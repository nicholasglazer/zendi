import React, {useCallback} from 'react'
import {useDropzone} from 'react-dropzone'
import s from '@emotion/styled'
import localforage from 'localforage'

function FilePicker(props) {
  const onDrop = useCallback((acceptedFiles) => {
    console.log('ondrop fied')
    acceptedFiles.forEach((file) => {
      const reader = new FileReader()
      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onloadend = () => {
        console.log('ondrop onloaded')
        const binaryStr = reader.result
        console.log(binaryStr)
        localforage.setItem('pdf', binaryStr)
        props.updatePdf()
      }
      reader.readAsArrayBuffer(file)
    })
  }, [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

  return (
    <Cont {...getRootProps()}>
      <input {...getInputProps()} />
      {
        isDragActive ?
        <p>Drop the files here ...</p> :
        <p>Drag 'n' drop files here, or click to select files</p>
      }
    </Cont>
  )
}
const Cont = s.div`
  padding: 24px;
  cursor: pointer;
  box-shadow: 0 0 12px #333;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 128px;
  transition: all .37s ease-out;
  &:hover {
  box-shadow: 0 0 4px #333;
  }
  >p { color: #ff2b87;}
`

export default FilePicker
