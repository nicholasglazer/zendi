import React from 'react'
import { Document, Page } from 'react-pdf'
import s from '@emotion/styled'

class PdfViewer extends React.Component {
  constructor(props) {
    super(props)
  }
  state = {
    numPages: null,
    pageNumber: 1,
  }

  onDocumentLoadSuccess = (props) => {
    this.setState({ numPages: props.numPages });
  }

  render() {
    const { pageNumber, numPages } = this.state;

    return (
      <Cont>
        <Document
          file={this.props.pdfData}
          options={{ maxImageSize: 1}}
          renderMode={'svg'}
          onLoadProgress={({ loaded, total }) => console.log('Loading a document: ' + (loaded / total) * 100 + '%')}
          loading={<div>Please wait</div>}
          noData={<div>Please select a file...</div>}

          onLoadSuccess={this.onDocumentLoadSuccess}
        >
          <Page
            pageNumber={pageNumber}
            width={this.props.width}
          />
        </Document>
        <p>Page {pageNumber} of {numPages}</p>
      </Cont>
    );
  }
}
const Cont = s.div`
  .react-pdf__Page {
    box-shadow: 0 30px 40px 0 rgba(16, 36, 94, 0.2);
    background: black;
  }

`
export default PdfViewer
