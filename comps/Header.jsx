import s from '@emotion/styled'

const Header = () => {
  return (
    <Wrapper>
      <span className="pink">
        ZENDI
      </span>
    </Wrapper>
  )
}

const Wrapper = s.div`
  background: #43464b;
  display: flex;
  flex: 1 100%;
  height: 36px;
  box-shadow: 0px 0px 8px rgba(0,0,0, .4);
  z-index: 1;
  > span {
    padding: 8px;
  }
`

export default Header
