import SideNav from './SideNav'
import Header from './Header'
import s from '@emotion/styled'

const LayoutStyle = s.div`
  display: flex;
  flex-wrap: wrap;
  height: auto;
  @media screen and (max-width: 796px) {
    flex-direction: column;
  }
`

const withLayout = Page => {
  return () => (
    <LayoutStyle>
      <Header />
      <SideNav />
      <PageWrapper>
        <Page />
      </PageWrapper>

      <style jsx global>{`
          html {
           height: 100vh;
           width: 100vw;
          }
          body {
           height: 100%;
           width: 100%;
           margin: 0;
           padding: 0;
           background: #43464b;
           font-family: 'Arial';
          }
          #__next {height: inherit; width: inherit;}
          a, p, div {color: #f6f5f3;}
          .pink { color: #ff2b87;}
        `}</style>
    </LayoutStyle>
  )
}

const PageWrapper = s.div`
  margin: 24px;
  flex: 1;
`

export default withLayout
