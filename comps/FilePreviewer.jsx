import s from '@emotion/styled'
import localforage from 'localforage'
import Pdf from '../comps/PDF'

class FilePreviewer extends React.Component {
  constructor(props) {
    super(props)
    this.state ={
      file: null
    }
  }
  render() {
    console.log('state.file', this.props)
    return (
      <Cont>
        {
          this.props.pdf
          ? (<Pdf pdfData={this.props.pdf} width={this.props.width} />)
          : (<p> Nothing to preview yet... </p>)
        }
      </Cont>
    )
  }
}

const Cont = s.div`
  border: 1px solid #5f5f5f;
  box-shadow: inset 0 0 20px rgba(56,56,56, .6);
  padding: 48px 0;
  min-height: 60vh;
  min-width: 200px;
  max-width: 300px;
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  > p {
  color: #8f8f8f;
  text-shadow:  -1px 0px 1px rgba(20,20,20, .7);
  }

`

export default FilePreviewer
