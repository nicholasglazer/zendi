import Link from "next/link"
import s from "@emotion/styled"

const SideNav = () => (
  <SideNavWrapper>
    <Link href="/preview" passHref>
      <SideNavLinkText>
        Preview
      </SideNavLinkText>
    </Link>
    <Link href="/results" passHref>
      <SideNavLinkText>
        Results
      </SideNavLinkText>
    </Link>
  </SideNavWrapper>
)

const SideNavWrapper = s.div`
  display: flex;
  background: black;
  flex: 1;
  background: #f7f8f9;
  max-width: 224px;
  min-width: 224px;
  box-shadow: inset -4px 0px 10px rgba(80,90,100, .4);
  justify-content: flex-start;
  align-items: flex-start;
  flex-wrap: nowrap;
  flex-direction: column;
  @media screen and (max-width: 796px) {
    max-width: 100%;
    min-width: 100%;
    flex: 0;
  }
`

const SideNavLinkText = s.a`
  display: block;
  color: #6a7a7a;
  text-decoration: none;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 36px;
  padding: 2px 0;
  vertical-align: middle;
  transition: all .3s ease-in-out;
  &:hover {
    color: #f4f6f7;
    background: #6a7a7a;
  }
  @media screen and (max-width: 796px) {
     &:not(:first-of-type) {
       border-top: 1px solid #6a7a7a;
     }
  }
`

export default SideNav
