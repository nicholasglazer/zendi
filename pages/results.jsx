import React from 'react'
import withLayout from '../comps/Layout'
import s from '@emotion/styled'
import localforage from 'localforage'
import Pdf from '../comps/PDF'


class Results extends React.Component {
  constructor() {
    super()
    this.state ={
      file: null
    }
  }
  getFile = async () => {
    try {
      const value = await localforage.getItem('pdf')
      this.setState({file: value})
    } catch(err) {
      console.log(err)
    }
  }
  componentDidMount() {
    this.getFile()
  }
  render() {
    console.log('state.file', this.state.file)
    return (
      <Cont>
        <div>
          <Pdf pdfData={this.state.file} width={500} />
        </div>
      </Cont>
    )
  }
}

const Cont = s.div`
  flex: 1;
`

export default withLayout(Results)
