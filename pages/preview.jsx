import withLayout from '../comps/Layout'
import {key, fullUrlPost} from '../const.js'
import FilePicker from '../comps/FilePicker'
import FilePreviewer from '../comps/FilePreviewer'
import s from '@emotion/styled'
import localforage from 'localforage'
import React from 'react'

class Preview extends React.Component {
  constructor() {
    super()
    this.state = {
      file: null,
      res: 'Analyze to get results'
    }
  }

  updatePdf = () => {
    this.getFile()
  }
  analyzePdf = () => {
    this.uploadFileCloudML(this.state.file)
  }

  getFile = async () => {
    try {
      const value = await localforage.getItem('pdf')
      this.setState({file: value})
    } catch(err) {
      console.log(err)
    }
  }

  componentDidMount() {
    this.getFile()
  }

  uploadFileCloudML = async (file) => {
    const myHeaders = new Headers({
      "Content-Type": "application/pdf",
      "Ocp-Apim-Subscription-Key": key
    })
    const myPostInit = {
      method: 'POST',
      headers: myHeaders,
      body: file,
      mode: 'cors',
      cache: 'default'
    }
    const myGetInit = {
      method: 'GET',
      headers: myHeaders,
      mode: 'cors',
      cache: 'default'
    }
    try {
      const response = await fetch(fullUrlPost, myPostInit)
      const OLURL = response.headers.get('Operation-Location')
      const results = await fetch(OLURL, myGetInit)
      const json = await results.json()
      this.setState({res: JSON.stringify(json, null, 2)})
    } catch (error) {
      this.setState({res: error})
      console.log('ERERORRORER', error)
    }
  }
  render() {
    return (
      <Cont>
        <BtnWrapper>
          <FilePicker updatePdf={this.updatePdf} />
          {
            this.state.file ? (
              <>
              <ResultStatus>
                {this.state.res}
              </ResultStatus>
              <AnalyzeButton onClick={this.analyzePdf}>
                <span>
                  Analyze
                </span>
              </AnalyzeButton>
              </>
            ) : null
          }
        </BtnWrapper>
        <FilePreviewer width={240} pdf={this.state.file} />
      </Cont>
    )
  }
}


const Cont = s.div`
  display: flex;
  flex: 1 100%;
  justify-content: space-around;
  width: 100%;
`
const BtnWrapper = s.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-itmes: center;
  justify-content: space-between;
  margin-right: 16px;
`
const ResultStatus = s.div`
  border: 1px dashed #667778;
  padding: 20px;
`
const AnalyzeButton = s.div`
  width: 168px;
  height: 60px;
  color: #ff2b87;
  font-size: 19px;
  box-shadow: 0 0 10px rgba(20,20,20, .4);
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition: all .37s ease-out;
  &:hover {box-shadow: 0 0 4px #333;}
  &:active, &:focus { background: #f4f5f7; color: #434343; box-shadow: inset 1px 1px 4px #000;}
  > span {text-shadow: -1px 0 0 rgba(25,25,25, .1); font-size: 20px;}
`

export default withLayout(Preview)
