export const key = '71e38f8080724e689fa5a27b6d64b6fc'
export const modelId = '88bdb4b3-fd18-4aad-a1df-bbf39876a6ea'
export const endpoint = 'westus2.api.cognitive.microsoft.com'
export const fullUrlPost = `https://${endpoint}/formrecognizer/v2.0-preview/custom/models/${modelId}/analyze?includeTextDetails={true}`


// operation location api
// https://cognitiveservice/formrecognizer/v2.0-preview/custom/models/{modelId}/analyzeResults/{resultId}
